<?php

class Banner
{
	/**
	 * @var BannerRepository
	 */
	private $repository;

	/**
	 * Banner constructor
	 * @param BannerRepository $repository
	 */
	public function __construct(BannerRepository $repository)
	{
		$this->repository = $repository;
	}

	/**
	 * Get banners data
	 * @return mixed
	 * @throws Exception
	 */
	public function get()
	{
		$data = [];
		$bannerId = 10;
		$banners = $this->repository->get();

		if (empty($banners)) {
			throw new Exception('Banners are empty');
		}

		foreach ($banners as $banner) {
			if ($banner['id'] == $bannerId) {
				continue;
			}

			$data[] = $banner;
		}

		return $data;
	}
}